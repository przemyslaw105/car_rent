<?php

namespace Src\Controllers;

use Src\Models\User;
use PDO;

class Home extends Controller
{
  public function connecting()
  {
    try{
      $handler = new PDO('mysql:host=127.0.0.1;port=1099;dbname=car_rent', 'root', '');
      $handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $e) {
      echo $e->getMessage();
      die();
    }
  }

  public function register($name ='')
  {
    $user = $this->model('User');
    $user->name = $name;
    $this->view('home/register');
  }

  public function memberRegister(){
    try{
      $handler = new PDO('mysql:host=127.0.0.1;port=1099;dbname=car_rent', 'root', '');
      $handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $e) {
      echo $e->getMessage();
      die();
    }

    $name = $_POST["name"];
    $username = $_POST["username"];
    $password = $_POST["password"];
    $isadmin = $_POST["isadmin"];
    $_SESSION["name"] = $name;
    $_SESSION["username"] = $username;
    $_SESSION["password"] = $password;
    $_SESSION["isadmin"] = $isadmin;


    if ($isadmin != 1)
    {
      $isadmin = 0;
    }

    $sql = "INSERT INTO users (name, username, password, isadmin) VALUES (?, ?, ?, ?)";
    $query = $handler->prepare($sql);

    $query->execute(array($name, $username, $password, $isadmin));

    header('location: login');
  }



  public function login($name ='')
  {
    $user = $this->model('User');
    $user->name = $name;
    $this->view('home/login');
  }

  public function loginStep($name ='')
  {
    try{
      $handler = new PDO('mysql:host=127.0.0.1;port=1099;dbname=car_rent', 'root', '');
      $handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $e) {
      echo $e->getMessage();
      die();
    }

    try {
      $helpstring = "'" . $_POST['username'] . "'";

      $query = $handler->prepare('SELECT password FROM users WHERE username = ' . $helpstring);
      $query->execute();
      $result = $query->fetch(PDO::FETCH_ASSOC);

      if($_POST['password'] == $result['password'])
        {
        session_start();
        $_SESSION['username'] = $_POST['username'];
        header('location: userpage');
        } else
        {
          echo "Invalid password or username";
        }
    } catch (PDOException $e) {
    echo "Something's wrong. Try again later.";
    }
  }

  public function userpage($name ='')
  {
    $user = $this->model('User');
    $user->name = $name;
    $this->view('home/userpage');
  }

  public function logout()
  {
    session_start();
    session_destroy();
    header('location: login');
  }

  public function member($name="")
  {
    $user = $this->model('User');
    $user->name = $name;
    $this->view('home/member');
  }


  public function index($name = '')
  {
    $user = $this->model('User');
    $user->name = $name;

    $this->view('home/index', ['name' => $user->name]);
  }
}

?>
