<?php

namespace Src\Controllers;
use Src\Models\User;
abstract class Controller
{
  protected $view;
  protected $data;

  public function model($model)
    {
      require_once SRC . 'Models' . DS . $model .'.php';
      $nameSPpath = 'Src\Models';
      $helpmodel = $nameSPpath . DS . $model;
      return new $helpmodel();
    }

    public function view($view, $data='')
    {
      require_once SRC . 'views' . DS . $view .'.php';
    }


}
?>
