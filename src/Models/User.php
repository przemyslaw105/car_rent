<?php

namespace Src\Models;

use Src\Models\BasicModel;

class User extends BasicModel
{
    public $name;

    protected $username;

    protected $password;

    protected $isadmin;

// GETING

    public function getName() : string {
        return $this->name;
    }

    public function getUsername() : string {
        return $this->username;
    }

    public function getPassword() : string {
        return $this->password;
    }

    public function getIsadmin() : bool {
        return $this->isadmin;
    }

// SETING

    public function setName(string $name) {
       $this->name = $name;
  }

    public function setUsername(string $username) {
        $this->username = $username;
  }

    public function setPassword(string $password) {
        $this->password = $password;
  }

    public function setIsadmin(bool $isadmin) {
        $this->isadmin = $isadmin;
  }

    public function __construct()
    {
    }

    public function greet()
    {
      echo "This is user class";
    }



}

 ?>
