<?php

namespace Src\Models;

use Src\Models\BasicModel;

class Car extends BasicModel
{
  protected $name;

  protected $price;

  protected $date_of_add;

// GETING

  public function getName() : string {
      return $this->name;
  }

  public function getPrice() : integer {
      return $this->price;
  }

  public function getDate_of_add() : string {
      return $this->date_of_add;
  }


// SETING

  public function setName(string $name) {
     $this->name = $name;
   }

   public function setPrice(integer $price) {
      $this->price = $price;
    }

    public function setDate_of_add(string $date_of_add) {
      $this->date_of_add = $date_of_add;
    }

    public function __construct(){
      print "Car construct <br>";
    }


}
 ?>
